const express = require("express");
require("dotenv").config();

// const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

// Permet de gérer les requêtes venant d'un domaine différent.
app.use(cors());

// Utilisé pour parser le contenu des requêtes POST
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());

// Importer les routes déclarées dans le routeur
app.use(require("./routes"));

app.listen(process.env.PORT, () => {
  console.log(`Example app listening at http://localhost:${process.env.PORT}`);
});
